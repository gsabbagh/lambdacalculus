# LambdaCalculus

A Haskell tool to manipulate lambda expression in De Bruijn form. It was originally done to evaluate the lambda expression associated to closure to efficient causation (Rosen's concept transformed into a lambda expression in "A computable expression of closure to efficient causation" by Matteo Mossio, Giuseppe Longo and John Stewart).

## Table of Contents
3. [Installation](#installation)
4. [Collaboration](#collaboration)

## Installation

This tool only needs a Haskell compiler such as ghc and preferably cabal.

## Collaboration

Any input is appreciated.

## Usage

The documentation is in the doc folder, you can generate it with ```cabal haddock```.

The source code is in the src folder, you can compile the file inside by hand or using cabal.

You can call functions defined in the code with the command ```cabal repl```.

The main function is the 6 first step of the evaluation of the lambda expression associated to closure to efficient causation. THe expressions are written in Krivine notation, the parenthesis surrond the first member of application such that x applied to f is written (f) x.

By executing the main, you should get the following result :
```
(\y.(\x.(y) (x) x) \x.(y) (x) x) \x.(((x) A) x) (x) A
(\y.(\x.(((x) A) x) (x) A) (x) x) \y.(\x.(((x) A) x) (x) A) (x) x
(\x.(((x) A) x) (x) A) (\y.(\x.(((x) A) x) (x) A) (x) x) \y.(\x.(((x) A) x) (x) A) (x) x
((((\y.(\x.(((x) A) x) (x) A) (x) x) \y.(\x.(((x) A) x) (x) A) (x) x) A) (\y.(\x.(((x) A) x) (x) A) (x) x) \y.(\x.(((x) A) x) (x) A) (x) x) ((\y.(\x.(((x) A) x) (x) A) (x) x) \y.(\x.(((x) A) x) (x) A) (x) x) A
((((\x.(((x) A) x) (x) A) (\y.(\x.(((x) A) x) (x) A) (x) x) \y.(\x.(((x) A) x) (x) A) (x) x) A) (\y.(\x.(((x) A) x) (x) A) (x) x) \y.(\x.(((x) A) x) (x) A) (x) x) ((\y.(\x.(((x) A) x) (x) A) (x) x) \y.(\x.(((x) A) x) (x) A) (x) x) A
(((((((\y.(\x.(((x) A) x) (x) A) (x) x) \y.(\x.(((x) A) x) (x) A) (x) x) A) (\y.(\x.(((x) A) x) (x) A) (x) x) \y.(\x.(((x) A) x) (x) A) (x) x) ((\y.(\x.(((x) A) x) (x) A) (x) x) \y.(\x.(((x) A) x) (x) A) (x) x) A) A) (\y.(\x.(((x) A) x) (x) A) (x) x) \y.(\x.(((x) A) x) (x) A) (x) x) ((\y.(\x.(((x) A) x) (x) A) (x) x) \y.(\x.(((x) A) x) (x) A) (x) x) A
```