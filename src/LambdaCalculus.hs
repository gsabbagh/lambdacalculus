module LambdaCalculus 
(
    LambdaExpr(..),
    betaNormalOrder,
    normalize,
    pprint,
    main
)
where    
    -- | A Lambda expression with De Bruijn indices.
    --
    -- A good description of De Bruijn indices can be found on the following Wikipedia page : https://en.wikipedia.org/wiki/De_Bruijn_index
    --
    -- By convention, we use a negative number for free variables bounded to no lambda expression at all.
    data LambdaExpr = V Int
                    | Lambda LambdaExpr
                    | App LambdaExpr LambdaExpr deriving (Eq, Show)
         
    
    -- | Substitutes a lambda expression into another.
    substitute :: LambdaExpr -> LambdaExpr -> LambdaExpr
    substitute v u = step3
        where
            decrementFreeV (V n) depth = if n > depth && n > 0 then (V (n-1)) else (V n)
            decrementFreeV (Lambda x) depth = Lambda (decrementFreeV x (depth + 1))
            decrementFreeV (App x y) depth = (App (decrementFreeV x depth) (decrementFreeV y depth))
            
            increaseFreeV k (V n) depth = if n > depth && n > 0 then (V (n+k)) else (V n)
            increaseFreeV k (Lambda x) depth = Lambda (increaseFreeV k x (depth + 1))
            increaseFreeV k (App x y) depth= (App (increaseFreeV k x depth) (increaseFreeV k y depth))
            
            replaceBoundV (V n) depth = if n == depth then (V 0) else (V n)
            replaceBoundV (Lambda x) depth = Lambda (replaceBoundV x (depth + 1))
            replaceBoundV (App x y) depth = (App (replaceBoundV x depth) (replaceBoundV y depth))
            
            replaceDummyV (V n) depth = if n == 0 then increaseFreeV (depth-1) v 1 else (V n)
            replaceDummyV (Lambda x) depth = Lambda (replaceDummyV x (depth + 1))
            replaceDummyV (App x y) depth = (App (replaceDummyV x depth) (replaceDummyV y depth))
            
            step1 = replaceBoundV u 1
            step2 = decrementFreeV step1 1
            step3 = replaceDummyV step2 1
    
    -- | Executes a lambda transformation according to normal order strategy if it can.
    --
    -- Returns a couple, the first value is the new lambda expression and the second value is a boolean indicating if the expression was transformed.
    betaNormalOrder :: LambdaExpr -> (LambdaExpr, Bool)
    betaNormalOrder (V a) = ((V a),False)
    betaNormalOrder (Lambda expr) = let (newExpr, b) = betaNormalOrder expr in ((Lambda newExpr), b)
    betaNormalOrder (App (Lambda u) v) = (substitute v u, True)
    betaNormalOrder (App u v) = let (newU,b1) = betaNormalOrder u 
                                    (newV,b2) = betaNormalOrder v
                                in if b1 then ((App newU v), True) else ((App u newV), b2)
    
    -- | Normalizes a lambda expression, if the lambda expression given has no normal form, the function never terminates.
    normalize :: LambdaExpr -> LambdaExpr
    normalize expr = let (newExpr, b) = betaNormalOrder expr in 
                        if b then normalize newExpr else expr
    
    -- | Gives the height of a lambda abstraction (it's the maximum number of lambda abstraction below it).
    height :: LambdaExpr -> Int
    height (V _) = 0
    height (App u v) = max (height u) (height v)
    height (Lambda x) = (height x) + 1

    -- | Infinite list of letters in order to name variables in a lambda expression.
    letters :: [String]
    letters = concat (((:[]) <$> ['x'..'z']):[(:(show i)) <$> ['a'..'z'] | i <- [1..]])
    
    -- | Letters for free variables.
    lettersFreeVar :: [String]
    lettersFreeVar = (:[]) <$> ['A'..'Z']

    -- | Pretty print a lambda expression, using Krivine convention for parenthesis : function application is expressed as (f) x
    pprint :: LambdaExpr -> String
    pprint (V a) = if a > 0 then letters !! (a-1) else lettersFreeVar !! ((abs a) - 1)
    pprint (Lambda expr) = "\\"++(letters !! (height expr))++"."++pprint expr
    pprint (App u v) = "("++pprint u++") "++pprint v

    -- | The Y combinator.
    y :: LambdaExpr
    y = Lambda $ App (Lambda (App (V 2) (App (V 1) (V 1)))) (Lambda (App (V 2) (App (V 1) (V 1))))
    
    -- | The G formula of Rosen.
    g :: LambdaExpr
    g = Lambda $ (App (App (App (V 1) (V (-1))) (V 1)) (App (V 1) (V (-1))))
    
    -- | The f function in (M,R) systems.
    f = App y g
    
    -- | Prints several steps of the normalization attempt of the f function.
    --
    -- The result should be :
    --(\y.(\x.(y) (x) x) \x.(y) (x) x) \x.(((x) A) x) (x) A
    --
    --(\y.(\x.(((x) A) x) (x) A) (x) x) \y.(\x.(((x) A) x) (x) A) (x) x
    --
    --(\x.(((x) A) x) (x) A) (\y.(\x.(((x) A) x) (x) A) (x) x) \y.(\x.(((x) A) x) (x) A) (x) x
    --
    --((((\y.(\x.(((x) A) x) (x) A) (x) x) \y.(\x.(((x) A) x) (x) A) (x) x) A) (\y.(\x.(((x) A) x) (x) A) (x) x) \y.(\x.(((x) A) x) (x) A) (x) x) ((\y.(\x.(((x) A) x) (x) A) (x) x) \y.(\x.(((x) A) x) (x) A) (x) x) A
    --
    --((((\x.(((x) A) x) (x) A) (\y.(\x.(((x) A) x) (x) A) (x) x) \y.(\x.(((x) A) x) (x) A) (x) x) A) (\y.(\x.(((x) A) x) (x) A) (x) x) \y.(\x.(((x) A) x) (x) A) (x) x) ((\y.(\x.(((x) A) x) (x) A) (x) x) \y.(\x.(((x) A) x) (x) A) (x) x) A
    --
    --(((((((\y.(\x.(((x) A) x) (x) A) (x) x) \y.(\x.(((x) A) x) (x) A) (x) x) A) (\y.(\x.(((x) A) x) (x) A) (x) x) \y.(\x.(((x) A) x) (x) A) (x) x) ((\y.(\x.(((x) A) x) (x) A) (x) x) \y.(\x.(((x) A) x) (x) A) (x) x) A) A) (\y.(\x.(((x) A) x) (x) A) (x) x) \y.(\x.(((x) A) x) (x) A) (x) x) ((\y.(\x.(((x) A) x) (x) A) (x) x) \y.(\x.(((x) A) x) (x) A) (x) x) A
    main :: IO ()
    main = do
        sequence $ putStrLn.pprint <$> take 6 (iterate (fst.betaNormalOrder) f)
        return ()
        